import {Component, OnInit} from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
    <div class="app-root-loader" *ngIf="loading">
      <div class="app-root-loader__pic"></div>
      <span class="app-root-loader__text"></span>
    </div>

    <div class="app-router-wrap" *ngIf="!loading">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent implements OnInit {

  loading = true;

  constructor(public router: Router) {
  }

  ngOnInit(): void {
    this.router.events
      .subscribe(
        (event: RouterEvent) => {
          this.navigationInterceptor(event);
        }
      );
  }

  navigationInterceptor(event: RouterEvent): void {

    if (!this.loading && event instanceof NavigationStart) {
      this.loading = true;
    }

    if (this.loading
      && (event instanceof NavigationEnd
        || event instanceof NavigationCancel
        || event instanceof NavigationError)) {
      this.loading = false;
    }
  }
}
