import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

export class AppCommonStorageEntity extends DwfeAbstractStorageEntity {

  _lang: string;

  get lang(): string {
    return this._lang;
  }

  set lang(value: string) {
    this._lang = value;
    this.saveToStorage();
  }

  get storageKey(): string {
    return 'appCommonStorage';
  }

  static of(data: any): AppCommonStorageEntity {
    const obj = new AppCommonStorageEntity();

    obj._lang = data['lang'];

    obj.saveToStorage();

    return obj;
  }

  fromStorage(params?: any): AppCommonStorageEntity {
    if (this.fillFromStorage()) {

      this._lang = this.parsed._lang;

      return this;
    } else {
      return null;
    }
  }
}
