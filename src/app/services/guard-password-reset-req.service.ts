import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';

import {NevisService} from '@nevis/services/nevis.service';

@Injectable({
  providedIn: 'root'
})
export class AppGuardPasswordResetReqService implements CanActivate {

  constructor(public nevisService: NevisService,
              public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.nevisService.isLoggedIn) {
      this.router.navigateByUrl('/cp/account/password');
      return false;
    } else {
      return true;
    }
  }
}
