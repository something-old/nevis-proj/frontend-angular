import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';

import {NevisService} from '@nevis/services/nevis.service';

@Injectable({
  providedIn: 'root'
})
export class AppGuardAuthService implements CanActivate, CanLoad {

  constructor(public nevisService: NevisService,
              public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isLoggedInCondition();
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    return this.isLoggedInCondition();
  }

  isLoggedInCondition(): boolean {
    if (this.nevisService.isLoggedIn) {
      this.router.navigateByUrl('/cp');
      return false;
    } else {
      return true;
    }
  }
}
