import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';

import {NevisService} from '@nevis/services/nevis.service';

@Injectable({
  providedIn: 'root'
})
export class AppGuardControlPanelService implements CanActivate, CanLoad {

  constructor(public nevisService: NevisService,
              public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isLoggedInCondition();
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    return this.isLoggedInCondition();
  }

  isLoggedInCondition(): boolean {
    if (this.nevisService.isLoggedIn) {
      return true;
    } else {
      this.router.navigateByUrl('/auth');
      return false;
    }
  }
}
