import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable} from 'rxjs';

import {AppLang} from '../globals';
import {AppCommonStorageEntity} from '../classes/AppCommonStorageEntity';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  defaultLang = AppLang.EN;
  subjLang = new BehaviorSubject<string>(this.defaultLang);

  appCommonStorage: AppCommonStorageEntity;

  constructor() {
    this.init();
  }

  init(): void {
    this.appCommonStorage = new AppCommonStorageEntity().fromStorage();
    if (this.appCommonStorage && this.appCommonStorage.lang) {
      this.lang = this.appCommonStorage.lang;
    } else {
      this.appCommonStorage = AppCommonStorageEntity.of({lang: this.lang});
    }
  }

  get lang(): string {
    return this.subjLang.getValue();
  }

  get lang$(): Observable<string> {
    return this.subjLang.asObservable();
  }

  set lang(value: string) {
    value = Object.keys(AppLang).includes(value)
      ? value
      : this.defaultLang;

    this.subjLang.next(value);

    if (this.appCommonStorage && this.appCommonStorage.lang !== value) {
      this.appCommonStorage.lang = value;
    }
  }

  t7eMap(propName: string, langMap: any): string {
    if (langMap) {
      return this.t7eProp(langMap[propName]);
    }
    return 'UNDEFINED LANG MAP';
  }

  t7eProp(prop: any): string {
    let result = 'UNDEFINED LANG PROP';
    if (prop) {
      if (prop[this.lang]) {
        result = prop[this.lang];
      } else if (prop[this.defaultLang]) {
        result = prop[this.defaultLang];
      }
    }
    return result;
  }
}
