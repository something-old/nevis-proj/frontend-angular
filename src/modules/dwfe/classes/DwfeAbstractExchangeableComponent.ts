import {OnDestroy} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DwfeGlobals} from '../globals';
import {AppService} from '@app/services/app.service';

export abstract class DwfeAbstractExchangeableComponent implements OnDestroy, DwfeExchangeable {

  errorMessage = '';
  successMessage = '';

  isLocked = false;
  subjLocked = new Subject<boolean>();

  isCaptchaValid = false;
  subjCaptchaValid = new BehaviorSubject<boolean>(false);

  activeExchangeUnitNumber = 1; // use, if the page has multiple exchange blocks

  subjLatchForUnsubscribe = new Subject();

  focusOnInput = DwfeGlobals.focusOnDwfeInput;

  app: AppService;

  ngOnDestroy(): void {
    this.subjLatchForUnsubscribe.next();
  }

  get latchForUnsubscribe$(): Observable<any> {
    return this.subjLatchForUnsubscribe.asObservable();
  }

  setErrorMessage(value: any): void {
    if (typeof value === 'string') {
      this.errorMessage = value;
    } else {
      this.errorMessage = this.app.t7eProp(value);
    }
  }

  resetMessage(control: AbstractControl, fields): Subscription {
    return control.valueChanges
      .pipe(
        takeUntil(this.latchForUnsubscribe$)
      )
      .subscribe(
        () => {
          fields.forEach(field => {
            if (this[field] !== '') {
              this[field] = '';
            }
          });
        });
  }

  isLocked$(): Observable<boolean> {
    return this.subjLocked.asObservable();
  }

  setLocked(value: boolean): void {
    this.isLocked = value;
    this.subjLocked.next(value);
  }

  get isCaptchaValid$(): Observable<boolean> {
    return this.subjCaptchaValid.asObservable();
  }

  setCaptchaValid(value: boolean): void {
    this.isCaptchaValid = value;
    this.subjCaptchaValid.next(value);
  }
}

export interface DwfeExchangeable {
  setLocked(value: boolean): void;

  isLocked$(): Observable<boolean>;

  setErrorMessage(value: string): void;

  setCaptchaValid(value: boolean): void;
}
