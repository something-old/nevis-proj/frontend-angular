import {Observable} from 'rxjs';

export interface DwfeCanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

// == https://metanit.com/web/angular2/7.7.php
// == https://angular.io/guide/router#candeactivate-handling-unsaved-changes
