import {Observable} from 'rxjs';

import {DWFE_ENDPOINTS, OPT_FOR_ANONYMOUSE_REQ} from '../globals';
import {DwfeAbstractExchanger} from './DwfeAbstractExchanger';


export class DwfeGoogleCaptchaValidateExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      DWFE_ENDPOINTS.googleCaptchaValidate,
      body,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}
