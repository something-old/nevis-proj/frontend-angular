import {EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

export abstract class DwfeAbstractEditableControl implements OnInit, OnDestroy {

  group: FormGroup;
  control: AbstractControl;

  validators: any[];
  asyncValidators: any[];
  @Input() disableValidators = false;
  @Input() onlyValidatorRequired = false;

  @Output() takeGroup = new EventEmitter<FormGroup>();
  @Output() takeControl = new EventEmitter<AbstractControl>();

  @Input() labelText = '';
  @Input() hintText = '';
  @Input() appearanceValue = 'fill'; // 'fill', 'standard', 'outline', and ''
  @Input() tabIndexValue = 0;

  @Input() markIfChanged = false;
  @Input() initValue: any;
  isFirstChange = true;
  isChanged = false;
  compareAs = ''; // 'textField', 'dateField', and ''
  @Output() takeIsChanged = new EventEmitter<boolean>();

  @Input() cancelEdit$: Observable<boolean>;
  @Input() saveEdit$: Observable<boolean>;

  subjLatchForUnsubscribe = new Subject();

  ngOnInit(): void {
    this.group = new FormGroup({'ctrl': new FormControl('', this.validators, this.asyncValidators)});
    if (this.onlyValidatorRequired) {
      this.group = new FormGroup({'ctrl': new FormControl('', [Validators.required])});
    }
    if (this.disableValidators) {
      this.group = new FormGroup({'ctrl': new FormControl('')});
    }
    this.control = this.group.get('ctrl');

    this.control
      .valueChanges
      .pipe(takeUntil(this.latchForUnsubscribe$))
      .subscribe((value: any) => {
        if (this.canBeCompared(value)) {

          let isChanged = value !== this.initValue;

          if (this.compareAs === 'textField') {
            if (!this.initValue && value === '') {
              isChanged = false; // if init=null and has been changed to ''
            }
          } else if (this.compareAs === 'dateField') {
            if (value && this.initValue) {
              isChanged = value.setHours(0, 0, 0, 0) !== this.initValue.setHours(0, 0, 0, 0);
            }
          }
          this.setIsChanged(isChanged);
        }
      });

    if (this.cancelEdit$) {
      this.cancelEdit$
        .pipe(takeUntil(this.latchForUnsubscribe$))
        .subscribe(
          value => this.cancelEdit(value)
        );
    }

    if (this.saveEdit$) {
      this.saveEdit$
        .pipe(takeUntil(this.latchForUnsubscribe$))
        .subscribe(
          value => this.saveEdit(value)
        );
    }

    this.takeGroup.emit(this.group);
    this.takeControl.emit(this.control);
  }

  ngOnDestroy(): void {
    this.subjLatchForUnsubscribe.next();
  }

  get latchForUnsubscribe$(): Observable<any> {
    return this.subjLatchForUnsubscribe.asObservable();
  }

  canBeCompared(value): boolean {
    if (this.isFirstChange) {
      this.isFirstChange = false;
      this.initValue = value;
      return false;
    }
    return true;
  }

  setIsChanged(value): void {
    this.isChanged = value;
    this.takeIsChanged.emit(value);
  }

  cancelEdit(value): void {
    if (value) {
      this.control.setValue(this.initValue);
    }
  }

  saveEdit(value): void {
    if (value) {
      this.initValue = this.control.value;
      this.control.setValue(this.initValue);
    }
  }
}
