import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeCloseActionModule} from '../../../components/action/close/close';

import {DwfeSimpleDialogComponent} from './simple-dialog.component';

@NgModule({
  declarations: [
    DwfeSimpleDialogComponent,
  ],
  imports: [
    CommonModule,

    DwfeCloseActionModule,
  ],
  exports: [
    DwfeSimpleDialogComponent,
  ]
})
export class DwfeSimpleDialogModule {
}
