import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'dwfe-ok-action',
  templateUrl: './ok.component.html',
  styleUrls: ['./ok.component.scss']
})
export class DwfeOkActionComponent {

  @Input() isDisabled = false;

  @Input() showErrorMessage = false;
  @Input() showSuccessMessage = false;

  @Input() errorMessage: string;
  @Input() successMessage: string;

  @Input() Label = 'Ok';

  @Input() type = 'standard';

  @Output() takeClick = new EventEmitter<boolean>();
}
