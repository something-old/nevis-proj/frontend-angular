import {Component} from '@angular/core';

@Component({
  selector: 'dwfe-spinner-dotted-circle',
  templateUrl: './spinner-dotted-circle.component.html',
  styleUrls: ['./spinner-dotted-circle.component.scss']
})
export class DwfeSpinnerDottedCircleComponent {
}
