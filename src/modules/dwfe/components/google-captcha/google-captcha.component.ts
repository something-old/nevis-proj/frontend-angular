import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'dwfe-google-captcha',
  templateUrl: './google-captcha.component.html',
  styleUrls: ['./google-captcha.component.scss']
})
export class DwfeGoogleCaptchaComponent implements OnInit {

  @Input() captchaTxt = '';
  @Input() errorMessage: string;
  @Input() showOverlay = false;

  @Output() googleResponse = new EventEmitter<string>();

  ngOnInit() {
    this.googleResponse.emit(null);
  }
}
