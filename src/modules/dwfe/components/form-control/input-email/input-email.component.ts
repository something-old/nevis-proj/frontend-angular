import {Component, Input, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';

import {AppService} from '@app/services/app.service';

import {T7E_DWFE_INPUT_EMAIL} from '../../../t7e';
import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';
import {DwfeMatErrorStateMatcher} from '../../../classes/DwfeMatErrorStateMatcher';

@Component({
  selector: 'dwfe-input-email',
  templateUrl: './input-email.component.html',
})
export class DwfeInputEmailComponent extends DwfeAbstractEditableControl implements OnInit {

// http://emailregex.com/
  PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  @Input() maxLength = 100;

  @Input() reverseHandleRespFromBackend = false;

  @Input() externalBackendValidator = null;

  validators = [
    Validators.required,
    Validators.pattern(this.PATTERN),
    Validators.maxLength(this.maxLength),
  ];

  asyncValidators = [
    this.backendValidator.bind(this),
  ];

  compareAs = 'textField';

  matcher = new DwfeMatErrorStateMatcher();

  constructor(public app: AppService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }

  backendValidator() {
    return this.externalBackendValidator ?
      this.externalBackendValidator(this.control.value, this.reverseHandleRespFromBackend)
      : null;
  }

  get t7e(): any {
    return T7E_DWFE_INPUT_EMAIL;
  }
}
