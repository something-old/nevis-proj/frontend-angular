import {Component, OnInit} from '@angular/core';

import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

@Component({
  selector: 'dwfe-input-text',
  templateUrl: './input-text.component.html',
})
export class DwfeInputTextComponent extends DwfeAbstractEditableControl implements OnInit {

  compareAs = 'textField';

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }
}
