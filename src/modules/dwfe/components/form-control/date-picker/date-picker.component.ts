import {Component, OnInit} from '@angular/core';

import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

@Component({
  selector: 'dwfe-date-picker',
  templateUrl: './date-picker.component.html',
})
export class DwfeDatePickerComponent extends DwfeAbstractEditableControl implements OnInit {

  compareAs = 'dateField';

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }
}
