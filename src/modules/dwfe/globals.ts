import {ElementRef} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

import {DWFE_EXCHANGE_ERROR_CODES_MAP} from './t7e';

const PREFIX = '/DWFE/';

export const DWFE_ENDPOINTS = {
  googleCaptchaValidate: `${PREFIX}google-captcha-validate`,
};

export const DWFE_GOOGLE_CAPTCHA_INIT = {
  siteKey: '6LeKEm4UAAAAALQvPIr-8Kgl0ls_tWKXyH2A5Y63' // https://www.google.com/recaptcha/admin
};

const DWFE_EXCHANGE_ERROR_FOR_LOGOUT = [
  'invalid_client',
  'invalid_grant',
  'invalid_token',
  'unauthorized',
  'unauthorized_client',
  'unauthorized_user',
  'unsupported_grant_type'
];

export const OPT_FOR_ANONYMOUSE_REQ = {
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
};


export class DwfeGlobals {

  static optForAuthorizedReq(accessToken: string) {
    return {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + accessToken
      }
    };
  }

  static focusOnDwfeInput(elementRef: ElementRef) {
    elementRef.nativeElement.querySelector('.dwfe-form-group-material input').focus();
  }

  static randomStr(requiredStringLength,
                   prefix = '',
                   postfix = ''): string { // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
    let result = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    for (let i = 0; i < requiredStringLength; i++) {
      result += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return prefix + result + postfix;
  }

  static formatDate(date: Date) { // https://stackoverflow.com/questions/2013255/how-to-get-year-month-day-from-a-date-object
    const yyyyD = date.getFullYear();
    const mmD = (date.getMonth() + 1);
    const ddD = date.getDate();
    let yyyy = yyyyD + '';
    let mm = mmD + '';
    let dd = ddD + '';

    if (yyyyD < 1000) {
      yyyy = '0' + yyyy;
    }
    if (yyyyD < 100) {
      yyyy = '0' + yyyy;
    }
    if (yyyyD < 10) {
      yyyy = '0' + yyyy;
    }

    if (mmD < 10) {
      mm = '0' + mm;
    }

    if (ddD < 10) {
      dd = '0' + dd;
    }

    return yyyy + '-' + mm + '-' + dd;
  }

  static dateFromObj(obj: any, prop: string): Date {
    if (obj.hasOwnProperty(prop)) {
      const value = obj[prop];
      if (value) {
        return new Date(value);
      }
    }
    return null;
  }

  static prepareReq(value: any): any {
    return JSON.stringify(value);
  }

  static parseExchangeError(obj: HttpErrorResponse): any {

    let description: any;

    const error = obj.error;
    const status: number = obj.status; // 400, 500, etc.
    const statusText = obj.statusText;

    // if (status) {
    //   description += `${status}: `;
    // }


    // Error Option 1.
    // Real Http error
    // ===============
    // error: "..."
    //
    // Error Option 2.
    // Most likely an internal DWFE server error
    // =========================================
    // error: {
    //   error: "error_code",
    //   error_description: "..."
    // }

    let errorCode = error && error.hasOwnProperty('error') && error['error'] || null;
    const errorDescription = error && error.hasOwnProperty('error_description') && error['error_description'] || null;

    if (errorDescription && errorDescription === 'Bad credentials') {
      errorCode = 'bad_credentials';
    } else if (status === 504) {
      errorCode = 'gateway_timeout';
    }

    if (errorCode) {
      if (DWFE_EXCHANGE_ERROR_CODES_MAP.hasOwnProperty(errorCode)) {
        description = DWFE_EXCHANGE_ERROR_CODES_MAP[errorCode];
      } else if (errorDescription) {
        description = errorDescription;
      } else {
        description = errorCode;
      }
    } else if (statusText) {
      description = statusText;
    } else if (error) {
      errorCode = error;
      description = error;
    } else {
      description = DWFE_EXCHANGE_ERROR_CODES_MAP['something_went_wrong'];
    }

    return {
      errorCode: errorCode,
      description: description
    };
  }

  static isReasonForLogoutHttpErrResp(obj: HttpErrorResponse): boolean {
    if (obj.error) {
      const isOAuth2Error = obj.error.hasOwnProperty('error');
      const errorCode = isOAuth2Error ? obj.error.error : obj.error;
      return DwfeGlobals.isReasonForLogout(errorCode);
    } else {
      return false;
    }
  }

  static isReasonForLogout(errorCode: string): boolean {
    return DWFE_EXCHANGE_ERROR_FOR_LOGOUT.includes(errorCode);
  }

  static appendChildScript(src: string, defer = false, elem: Element, onLoadFn: any) {

    const gScript = document.createElement('script');

    gScript.src = src;
    gScript.async = true;
    gScript.defer = defer;
    gScript.onload = onLoadFn;

    elem.appendChild(gScript);
  }

  static triggerChangeDetection(): void {

    // sometimes Angular doesn't want to see a change :(
    //
    // issue: https://github.com/angular/angular/issues/24181
    // test:  https://github.com/MyTempGitForCommunity/angular6-ngIf-not-working
    //

    window.dispatchEvent(new Event('resize'));

    // also you can use:
    // constructor(appRef: ApplicationRef) {
    // }
    // ...
    // this.appRef.tick();
  }

  static regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    //
    // == https://stackoverflow.com/questions/48843538/how-to-differentiate-multiple-validators-pattern#48844043
    //
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  }

  static isEscapeKey(evt): boolean {

    let result = false;

    if ('key' in evt) {
      const key = evt.key.toLowerCase();
      result = key === 'escape' || key === 'esc';
    } else if ('keyCode' in evt) {
      result = evt.keyCode === 27;
    }
    return result;
  }

  static listValueScan(arr, value): any {
    for (const item of arr) {
      if (value === item.value) {
        return item.viewValue;
      }
    }
    return false;
  }
}
