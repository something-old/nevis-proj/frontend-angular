import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

export class NevisAccountPersonalBackendEntity extends DwfeAbstractStorageEntity {

  nickName: string;
  nickNameNonPublic: boolean;

  firstName: string;
  firstNameNonPublic: boolean;

  middleName: string;
  middleNameNonPublic: boolean;

  lastName: string;
  lastNameNonPublic: boolean;

  gender: string;
  genderNonPublic: boolean;

  dateOfBirth: Date;
  dateOfBirthNonPublic: boolean;

  country: string;
  countryNonPublic: boolean;

  city: string;
  cityNonPublic: boolean;

  company: string;
  companyNonPublic: boolean;

  positionHeld: string;
  positionHeldNonPublic: boolean;

  updatedOn: Date;

  get storageKey(): string {
    return 'nevisAccountPersonal';
  }

  static of(data, isOAuthClientUntrusted: boolean): NevisAccountPersonalBackendEntity {

    const obj = new NevisAccountPersonalBackendEntity();

    obj.nickName = data['nickName'];
    obj.nickNameNonPublic = data['nickNameNonPublic'];

    obj.firstName = data['firstName'];
    obj.firstNameNonPublic = data['firstNameNonPublic'];

    obj.middleName = data['middleName'];
    obj.middleNameNonPublic = data['middleNameNonPublic'];

    obj.lastName = data['lastName'];
    obj.lastNameNonPublic = data['lastNameNonPublic'];

    obj.gender = data['gender'];
    obj.genderNonPublic = data['genderNonPublic'];

    obj.dateOfBirth = DwfeGlobals.dateFromObj(data, 'dateOfBirth');
    obj.dateOfBirthNonPublic = data['dateOfBirthNonPublic'];

    obj.country = data['country'];
    obj.countryNonPublic = data['countryNonPublic'];

    obj.city = data['city'];
    obj.cityNonPublic = data['cityNonPublic'];

    obj.company = data['company'];
    obj.companyNonPublic = data['companyNonPublic'];

    obj.positionHeld = data['positionHeld'];
    obj.positionHeldNonPublic = data['positionHeldNonPublic'];

    obj.updatedOn = DwfeGlobals.dateFromObj(data, 'updatedOn');

    obj.saveToStorage(isOAuthClientUntrusted);
    return obj;
  }

  fromStorage(): NevisAccountPersonalBackendEntity {

    if (this.fillFromStorage()) {
      this.nickName = this.parsed.nickName;
      this.nickNameNonPublic = this.parsed.nickNameNonPublic;

      this.firstName = this.parsed.firstName;
      this.firstNameNonPublic = this.parsed.firstNameNonPublic;

      this.middleName = this.parsed.middleName;
      this.middleNameNonPublic = this.parsed.middleNameNonPublic;

      this.lastName = this.parsed.lastName;
      this.lastNameNonPublic = this.parsed.lastNameNonPublic;

      this.gender = this.parsed.gender;
      this.genderNonPublic = this.parsed.genderNonPublic;

      this.dateOfBirth = DwfeGlobals.dateFromObj(this.parsed, 'dateOfBirth');
      this.dateOfBirthNonPublic = this.parsed.dateOfBirthNonPublic;

      this.country = this.parsed.country;
      this.countryNonPublic = this.parsed.countryNonPublic;

      this.city = this.parsed.city;
      this.cityNonPublic = this.parsed.cityNonPublic;

      this.company = this.parsed.company;
      this.companyNonPublic = this.parsed.companyNonPublic;

      this.positionHeld = this.parsed.positionHeld;
      this.positionHeldNonPublic = this.parsed.positionHeldNonPublic;

      this.updatedOn = DwfeGlobals.dateFromObj(this.parsed, 'updatedOn');

      return this;
    } else {
      return null;
    }
  }
}

