import {AfterViewInit, Component, ElementRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

import {takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {T7E_NEVIS_AUTH_LOGIN} from '../t7e';
import {NevisClientType} from '../../../globals';
import {NevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAuthLoginComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  cEmail: AbstractControl;
  cPassword: AbstractControl;
  cNotMyDevice: AbstractControl;

  @ViewChild('refEmail', {read: ElementRef}) refEmail: ElementRef;
  @ViewChild('refPassword', {read: ElementRef}) refPassword: ElementRef;

  isThirdPartySignedIn = false;

  constructor(public nevisService: NevisService,
              public activatedRoute: ActivatedRoute,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {
    this.resetMessage(this.cEmail, ['errorMessage']);
    this.resetMessage(this.cPassword, ['errorMessage']);

    setTimeout(() => {

      this.cNotMyDevice.setValue(false);

      this.activatedRoute.queryParamMap.subscribe(map => {
        const username = map.get('username') || null;
        if (username) {
          this.cEmail.setValue(username);
        }
      });

      this.focus();

    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value) {
      this.focus();
    }
  }

  focus() {
    if (this.cEmail.invalid) {
      this.focusOnInput(this.refEmail);
    } else if (this.cPassword.invalid) {
      this.focusOnInput(this.refPassword);
    }
  }

  performLogin(): void {

    this.setErrorMessage('');
    this.setLocked(true);

    this.nevisService
      .setRedirectUrl('/')
      .performLoginStandard(
        this.cEmail.value,
        this.cPassword.value,
        this.cNotMyDevice.value ? NevisClientType.UNTRUSTED : NevisClientType.TRUSTED
      )
      .loginResult$
      .pipe(
        takeUntil(this.isLocked$())
      )
      .subscribe(
        (data: DwfeExchangeResult) => {
          if (data.result) { // actions on success 'Login'
          } else {
            this.setErrorMessage(data.description);
          }
          this.setLocked(false);
        }
      );
  }

  receiveThirdPartySignInResult(value) {
    this.isThirdPartySignedIn = value;
    DwfeGlobals.triggerChangeDetection();
  }

  get t7e(): any {
    return T7E_NEVIS_AUTH_LOGIN;
  }
}
