import {AfterViewInit, Component, ElementRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {Router} from '@angular/router';

import {of} from 'rxjs';
import {concatMap, delay, takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {T7E_NEVIS_AUTH_CREATE_ACCOUNT} from '../t7e';
import {NevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-auth-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAuthCreateAccountComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  cEmail: AbstractControl;
  cTermsOfUse: AbstractControl;
  cPrivacyPolicy: AbstractControl;

  @ViewChild('refEmail', {read: ElementRef}) refEmail: ElementRef;

  showCreatedNewAccScreen = false;

  constructor(public nevisService: NevisService,
              public router: Router,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {

    this
      .isCaptchaValid$
      .pipe(
        takeUntil(this.latchForUnsubscribe$),
        concatMap(x => of(x)
          .pipe(
            delay(10))) // otherwise below cEmail, cPrivacyPolicy return undefined
      ).subscribe(
      isCaptchaValid => {
        if (isCaptchaValid) {
          this.resetMessage(this.cEmail, ['errorMessage']);
          this.cTermsOfUse.setValue(false);
          this.cPrivacyPolicy.setValue(false);
          this.focusOnInput(this.refEmail);
        }
      });
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value && this.refEmail) {
      this.focusOnInput(this.refEmail);
    }
  }

  performCreateAccount() {

    this.nevisService
      .createAccountExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          email: this.cEmail.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {    // actions on success 'Create account'
            this.showCreatedNewAccScreen = true;
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  goToLoginPage() {

    this.router.navigate(
      ['/auth/login'], {
        queryParams: {
          username: this.cEmail.value
        }
      });
  }

  get t7e(): any {
    return T7E_NEVIS_AUTH_CREATE_ACCOUNT;
  }
}
