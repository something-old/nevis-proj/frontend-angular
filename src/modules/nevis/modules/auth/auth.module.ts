import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeModule} from '@dwfe/dwfe.module';

import {NevisAuthCreateAccountComponent} from './create-account/create-account.component';
import {NevisAuthLoginComponent} from './login/login.component';

import {NevisAuthThirdPartyComponent} from './third-party/third-party.component';
import {NevisAuthGoogleComponent} from './third-party/google/google.component';
import {NevisAuthFacebookComponent} from './third-party/facebook/facebook.component';
import {NevisAuthVkontakteComponent} from './third-party/vkontakte/vkontakte.component';
import {NevisAuthTwitterComponent} from './third-party/twitter/twitter.component';

@NgModule({
  imports: [
    CommonModule,

    DwfeModule,
  ],
  declarations: [
    NevisAuthCreateAccountComponent,
    NevisAuthLoginComponent,

    NevisAuthThirdPartyComponent,
    NevisAuthGoogleComponent,
    NevisAuthFacebookComponent,
    NevisAuthVkontakteComponent,
    NevisAuthTwitterComponent,
  ],
  exports: [
    NevisAuthCreateAccountComponent,
    NevisAuthLoginComponent,
  ]
})
export class NevisAuthModule {
}
