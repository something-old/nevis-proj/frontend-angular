import {AfterViewInit, Component, ElementRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {of} from 'rxjs';
import {concatMap, delay, takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {T7E_NEVIS_PASSWORD_RESET} from '../../t7e';
import {NevisService} from '../../../../services/nevis.service';

@Component({
  selector: 'nevis-password-reset-req',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisPasswordResetReqComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  cEmail: AbstractControl;
  @ViewChild('refEmail', {read: ElementRef}) refEmail: ElementRef;

  isExchangeCompleted = false;

  constructor(public nevisService: NevisService,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {

    this
      .isCaptchaValid$
      .pipe(
        takeUntil(this.latchForUnsubscribe$),
        concatMap(x => of(x)
          .pipe(
            delay(10))) // otherwise below cEmail return undefined
      ).subscribe(
      isCaptchaValid => {
        if (isCaptchaValid) {
          this.resetMessage(this.cEmail, ['errorMessage']);
          this.focusOnInput(this.refEmail);
        } else {
          this.isExchangeCompleted = false;
        }
      });
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value && this.refEmail) {
      this.focusOnInput(this.refEmail);
    }
  }

  performPasswordResetReq() {

    this.nevisService
      .passwordResetReqExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          email: this.cEmail.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {    // actions on success 'Password Reset Request'
            this.isExchangeCompleted = true;
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  get t7e(): any {
    return T7E_NEVIS_PASSWORD_RESET;
  }
}
