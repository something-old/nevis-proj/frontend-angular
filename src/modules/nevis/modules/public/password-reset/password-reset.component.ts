import {AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {T7E_NEVIS_PASSWORD_RESET} from '../t7e';
import {NevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisPasswordResetComponent extends DwfeAbstractExchangeableComponent implements OnInit, AfterViewInit {

  confirmKey: string;

  cNewPassword: AbstractControl;
  cRepeatPassword: AbstractControl;

  @ViewChild('refNewPassword', {read: ElementRef}) refNewPassword: ElementRef;
  @ViewChild('refRepeatPassword', {read: ElementRef}) refRepeatPassword: ElementRef;

  isExchangeCompleted = false;
  username: string;

  constructor(public nevisService: NevisService,
              public activatedRoute: ActivatedRoute,
              public router: Router,
              public app: AppService) {
    super();
  }

  ngOnInit(): void {
    this.confirmKey = this.activatedRoute.snapshot.paramMap.get('confirmKey');
  }

  ngAfterViewInit(): void {
    this.resetMessage(this.cNewPassword, ['errorMessage']);
    this.resetMessage(this.cRepeatPassword, ['errorMessage']);

    setTimeout(() => {
      this.focus();
    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value) {
      this.focus();
    }
  }

  focus() {
    if (this.cNewPassword.invalid) {
      this.focusOnInput(this.refNewPassword);
    } else if (this.cRepeatPassword.invalid) {
      this.focusOnInput(this.refRepeatPassword);
    }
  }

  performPasswordReset() {

    if (this.cNewPassword.value !== this.cRepeatPassword.value) {
      this.setErrorMessage(this.app.t7eMap('different_passwords', this.t7e));
      return;
    }

    this.nevisService
      .passwordResetExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          key: this.confirmKey,
          newpass: this.cNewPassword.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {    // actions on success 'Password Reset'
            this.isExchangeCompleted = true;
            this.username = data.data.username;
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  goToLoginPage() {

    this.router.navigate(
      ['/auth/login'], {
        queryParams: {
          username: this.username
        }
      });
  }

  get t7e(): any {
    return T7E_NEVIS_PASSWORD_RESET;
  }
}
