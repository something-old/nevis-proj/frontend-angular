import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {T7E_NEVIS_EMAIL_CONFIRM} from '../t7e';
import {NevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-email-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class NevisEmailConfirmComponent extends DwfeAbstractExchangeableComponent implements OnInit, AfterViewInit {

  confirmKey: string;

  constructor(public nevisService: NevisService,
              public activatedRoute: ActivatedRoute,
              public app: AppService) {
    super();
  }

  ngOnInit(): void {
    this.confirmKey = this.activatedRoute.snapshot.paramMap.get('confirmKey');
  }

  ngAfterViewInit(): void {
    setTimeout(() => {

      if (this.nevisService.isEmailConfirmed()) {
        this.errorMessage = '';
        this.successMessage = this.app.t7eMap('email_already_confirmed', this.t7e);
      } else {
        this.nevisService
          .emailConfirmExchanger
          .run(this,
            DwfeGlobals.prepareReq({
              key: this.confirmKey
            }),
            (data: DwfeExchangeResult) => {
              if (data.result) {    // actions on success 'Email Confirmation'
                this.nevisService.setEmailConfirmed(true);
                this.successMessage = this.app.t7eMap('email_now_confirmed', this.t7e);
              } else {
                this.setErrorMessage(data.description);
              }
            });
      }
    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  get t7e(): any {
    return T7E_NEVIS_EMAIL_CONFIRM;
  }
}
