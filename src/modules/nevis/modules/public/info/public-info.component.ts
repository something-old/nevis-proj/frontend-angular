import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {AppService} from '@app/services/app.service';

import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {T7E_NEVIS_ACCOUNT_PUBLIC_INFO} from '../t7e';
import {NevisService} from '../../../services/nevis.service';
import {DwfeGlobals} from '@dwfe/globals';
import {NEVIS_COUNTRIES, NEVIS_GENDERS} from '@nevis/t7e';

@Component({
  selector: 'nevis-account-public-info',
  templateUrl: './public-info.component.html',
  styleUrls: ['./public-info.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAccountPublicInfoComponent extends DwfeAbstractExchangeableComponent implements OnInit, AfterViewInit {

  id: string;
  email: string;
  phone: string;

  nickName: string;
  firstName: string;
  middleName: string;
  lastName: string;
  gender: string;
  dateOfBirth: string;
  country: string;
  city: string;
  company: string;
  positionHeld: string;

  hiddenInformation: string;

  constructor(public nevisService: NevisService,
              public activatedRoute: ActivatedRoute,
              public app: AppService) {
    super();
  }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.hiddenInformation = this.app.t7eMap('hidden_information', this.t7e);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.performReadFromServer();
    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  performReadFromServer() {

    this.nevisService
      .publicAccountInfoExchanger
      .run(this,
        {id: this.id},
        (data: DwfeExchangeResult) => {
          this.email = data.result && data.data.email.value || this.hiddenInformation;
          this.phone = data.result && data.data.phone.value || this.hiddenInformation;

          this.nickName = data.result && data.data.personal.nickName || this.hiddenInformation;
          this.firstName = data.result && data.data.personal.firstName || this.hiddenInformation;
          this.middleName = data.result && data.data.personal.middleName || this.hiddenInformation;
          this.lastName = data.result && data.data.personal.lastName || this.hiddenInformation;
          this.gender = data.result && data.data.personal.gender && this.app.t7eProp(DwfeGlobals.listValueScan(NEVIS_GENDERS, data.data.personal.gender)) || this.hiddenInformation;
          this.dateOfBirth = data.result && data.data.personal.dateOfBirth || this.hiddenInformation;
          this.country = data.result && data.data.personal.country && this.app.t7eProp(DwfeGlobals.listValueScan(NEVIS_COUNTRIES, data.data.personal.country)) || this.hiddenInformation;
          this.city = data.result && data.data.personal.city || this.hiddenInformation;
          this.company = data.result && data.data.personal.company || this.hiddenInformation;
          this.positionHeld = data.result && data.data.personal.positionHeld || this.hiddenInformation;

          if (!data.result) {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  get t7e(): any {
    return T7E_NEVIS_ACCOUNT_PUBLIC_INFO;
  }
}
