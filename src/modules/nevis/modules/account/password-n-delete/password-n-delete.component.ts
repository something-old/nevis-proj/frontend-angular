import {AfterViewInit, Component} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {T7E_NEVIS_ACCOUNT_PASSWORD_N_DELETE} from '../t7e';
import {NevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-account-password-n-delete',
  templateUrl: './password-n-delete.component.html',
  styleUrls: ['./password-n-delete.component.scss']
})
export class NevisAccountPasswordNDeleteComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  cNewPassword: AbstractControl;
  cRepeatNewPassword: AbstractControl;
  cCurrentPasswordChange: AbstractControl;
  cCurrentPasswordDelete: AbstractControl;

  constructor(public nevisService: NevisService,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {

    this.resetMessage(this.cNewPassword, ['errorMessage', 'successMessage']);
    this.resetMessage(this.cRepeatNewPassword, ['errorMessage', 'successMessage']);
    this.resetMessage(this.cCurrentPasswordChange, ['errorMessage', 'successMessage']);
    this.resetMessage(this.cCurrentPasswordDelete, ['errorMessage', 'successMessage']);
  }

  performChangePassword() {
    this.activeExchangeUnitNumber = 1;

    if (this.cNewPassword.value !== this.cRepeatNewPassword.value) {
      this.setErrorMessage(this.app.t7eMap('different_passwords', this.t7e));
      return;
    }

    this.nevisService
      .passwordChangeExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          curpass: this.cCurrentPasswordChange.value,
          newpass: this.cNewPassword.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {
            this.cNewPassword.reset();
            this.cRepeatNewPassword.reset();
            this.cCurrentPasswordChange.reset();
            this.successMessage = this.app.t7eMap('success_changed', this.t7e);
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  performDeleteAccount() {
    this.activeExchangeUnitNumber = 2;

    this.nevisService
      .deleteAccountExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          curpass: this.cCurrentPasswordDelete.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {
            this.nevisService.logout();
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  get t7e(): any {
    return T7E_NEVIS_ACCOUNT_PASSWORD_N_DELETE;
  }
}
