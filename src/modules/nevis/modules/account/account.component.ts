import {Component, ViewEncapsulation} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {T7E_NEVIS_ACCOUNT} from './t7e';

@Component({
  selector: 'nevis-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAccountComponent {

  constructor(public app: AppService) {
  }

  get t7e(): any {
    return T7E_NEVIS_ACCOUNT;
  }
}
