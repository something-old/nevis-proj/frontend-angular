import {Component, OnInit} from '@angular/core';

import {APP_LANGUAGES} from '@app/globals';
import {AppService} from '@app/services/app.service';

import {T7E_APP_LANGUAGE} from '../t7e';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class AppLanguageComponent implements OnInit {

  items: { value: string, viewValue: string }[] = APP_LANGUAGES;

  selectedValue: string;

  showLangList = false;

  constructor(public app: AppService) {
  }

  ngOnInit() {
    this.selectedValue = this.app.lang;
  }

  openDialog() {
    setTimeout(() => {
      this.showLangList = true;
    }, 200);
  }

  select(value: string): void {
    this.selectedValue = value;
  }

  change(): void {
    if (this.isLangChanged()) {
      this.app.lang = this.selectedValue;
    }
    this.close();
  }

  close() {
    this.showLangList = false;
    if (this.isLangChanged()) {
      this.selectedValue = this.app.lang;
    }
  }

  isLangChanged(): boolean {
    return this.selectedValue !== this.app.lang;
  }

  get t7e(): any {
    return T7E_APP_LANGUAGE;
  }
}
