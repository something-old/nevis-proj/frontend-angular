import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {DwfeModule} from '@dwfe/dwfe.module';

import {NevisAuthModule} from '@nevis/modules/auth/auth.module';
import {NevisAuthCreateAccountComponent} from '@nevis/modules/auth/create-account/create-account.component';
import {NevisAuthLoginComponent} from '@nevis/modules/auth/login/login.component';

import {AppElementsModule} from '@app-mod/elements/elements.module';

import {AppLayoutAuthComponent} from './auth.component';

@NgModule({
  declarations: [
    AppLayoutAuthComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    AppElementsModule,

    NevisAuthModule,
    DwfeModule,

    RouterModule.forChild([
      {
        path: '', component: AppLayoutAuthComponent, children: [
          {path: '', pathMatch: 'full', redirectTo: 'login'},
          {path: 'create-account', component: NevisAuthCreateAccountComponent},
          {path: 'login', component: NevisAuthLoginComponent},
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ],
})
export class AppLayoutAuthModule {
}
