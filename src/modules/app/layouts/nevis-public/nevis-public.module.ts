import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {DwfeModule} from '@dwfe/dwfe.module';

import {NevisPublicModule} from '@nevis/modules/public/public.module';
import {NevisAccountPublicInfoComponent} from '@nevis/modules/public/info/public-info.component';
import {NevisEmailConfirmComponent} from '@nevis/modules/public/email-confirm/confirm.component';
import {NevisPasswordResetReqComponent} from '@nevis/modules/public/password-reset/request/request.component';
import {NevisPasswordResetComponent} from '@nevis/modules/public/password-reset/password-reset.component';

import {AppGuardPasswordResetReqService} from '@app/services/guard-password-reset-req.service';

import {AppElementsModule} from '@app-mod/elements/elements.module';

import {AppLayoutNevisPublicComponent} from './nevis-public.component';

@NgModule({
  declarations: [
    AppLayoutNevisPublicComponent
  ],
  imports: [
    CommonModule,

    AppElementsModule,
    NevisPublicModule,
    DwfeModule,

    RouterModule.forChild([
      {
        path: '', component: AppLayoutNevisPublicComponent, children: [
          {path: 'id/:id', component: NevisAccountPublicInfoComponent},
          {path: 'email-confirm/:confirmKey', component: NevisEmailConfirmComponent},
          {path: 'password-reset-req', canActivate: [AppGuardPasswordResetReqService], component: NevisPasswordResetReqComponent},
          {path: 'password-reset/:confirmKey', component: NevisPasswordResetComponent},
        ]
      },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppLayoutNevisPublicModule {
}
